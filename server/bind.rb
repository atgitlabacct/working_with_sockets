# Low level implementation to bind a socket to a local port.
# Other sockets can't bind to this port now
# However we are not yet listening on this socket
# There is Ruby syntatic sugar for this
require 'socket'

# First, create a new TCP socket.
socket = Socket.new(:INET, :STREAM)
puts socket.inspect
# Create a C struct to hold the address for listening
# We use 0.0.0.0 here to allow listening all all interfaces
# If we were to use the loopback only the socket would only
# be listening on that interface
addr = Socket.pack_sockaddr_in(4481, '0.0.0.0')
puts addr.inspect

# Bind to it
socket.bind(addr)
