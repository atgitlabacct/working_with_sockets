require "socket"

# Setup socket
server = Socket.new(:INET, :STREAM)
addr = Socket.pack_sockaddr_in(4481, '0.0.0.0')
server.bind(addr)
server.listen(Socket::SOMAXCONN) 


# Accept a connection
connection, _ = server.accept

# Note the connection.fileno is different then the server.fileno so that the
# server can continue handling new connections
print 'Connection class: '
p connection.class

print 'Connection fileno: '
p connection.fileno

print 'Server filno: '
p server.fileno

# Each TCP connection is defined by a unique grouping of local-host, local-port and
# remote-host, remote-port.  This combination must be unique for each TCP connection
print 'Local address: '
p connection.local_address

print 'Remote address: '
p connection.remote_address
