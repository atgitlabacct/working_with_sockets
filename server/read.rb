require 'socket'

Socket.tcp_server_loop(4481) do |connection|
  # Simplest way to read data from the connection
  puts connection.read

  # Close the connection once we're done reading
  # Lets the client know that they can stop waiting for us to write something back
  connection.close
end
