require 'socket'
require 'debugger'

# This wraps the creation of the socket, the binding and the listening
# This also returns a TCPServer instance, not Socket
server = TCPServer.new(4481)
puts server.inspect

# Returns the available sockets
servers = Socket.tcp_server_sockets(4481)
servers.each do |s|
  debugger
  puts s.inspect
end
