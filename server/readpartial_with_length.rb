require 'socket'
one_hundred_kb = 1024 * 100# bytes

Socket.tcp_server_loop(4481) do |connection|
  # Read data in chunks of 1 hundred kb or less
  begin
    while data = connection.readpartial(one_hundred_kb) do
      puts data 
    end
  rescue EOFError
  end

  # Close the connection once we're done reading
  # Lets the client know that they can stop waiting for us to write something back
  connection.close
end
