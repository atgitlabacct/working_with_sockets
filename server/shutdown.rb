require "socket"

# Setup socket
server = Socket.new(:INET, :STREAM)
addr = Socket.pack_sockaddr_in(4481, '0.0.0.0')
server.bind(addr)
server.listen(Socket::SOMAXCONN) 
connection, _ = server.accept

# Create a copy of the conection.
copy = connection.dup

# This shutsdown communication of all copies of the connection.
connection.shutdown

# This closes the original connection.  The copy will be closed
# when the GC collects it.
connection.close
