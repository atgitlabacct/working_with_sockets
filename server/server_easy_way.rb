require 'socket'
require 'debugger'

# This wraps the creation of the socket, the binding and the listening
# This also returns a TCPServer instance, not Socket
#
#server = TCPServer.new(4481)
#servers = Socket.tcp_server_sockets(4481)

# Enter an endless loop of accepting and handling connections.
# We can pass a collection of sockets into the accept_loop and
# Ruby handles them gracefully.

#Socket.accept_loop(server) do |connection|  # Handle one socket
#Socket.accept_loop(servers) do |connection| # Hanles multip sockets

# This is a wrapper around the Socket_server_sockets and Socket.accept_loop
Socket.tcp_server_loop(4481) do |connection| # Handle all sockets on port 4481
  # Handle connection
  puts 'I handled a connection'
  connection.close
end
