require "socket"

# Setup socket
server = Socket.new(:INET, :STREAM)
addr = Socket.pack_sockaddr_in(4481, '0.0.0.0')
server.bind(addr)
server.listen(Socket::SOMAXCONN) 


# Enter an endless loop of accepting and handling connections.
# This is so common that Ruby provides syntactic sugar on top of it
# Servers close connections to finished processing.
# Closing causes all open file descriptors to be closed for you.
# Close connections yourself so that you
#   1) don't store references to sockets no loger in use.
#     1a) The GC will close this when ran if you don't close it.
#   2) The system has an open file limit.  Remember in Unix everything is a file.
loop do
  connection, _ = server.accept
  # handle connection
  connection.close
end
