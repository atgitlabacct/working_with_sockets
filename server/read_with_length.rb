require 'socket'
one_kb = 1024 # bytes

Socket.tcp_server_loop(4481) do |connection|
  # Simplest way to read data from the connection
  while data = connection.read(one_kb) do
    puts data 
  end

  # Close the connection once we're done reading
  # Lets the client know that they can stop waiting for us to write something back
  connection.close
end
