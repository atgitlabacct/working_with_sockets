require "socket"

# Setup socket
server = Socket.new(:INET, :STREAM)
addr = Socket.pack_sockaddr_in(4481, '0.0.0.0')
server.bind(addr)
server.listen(Socket::SOMAXCONN) 


# Accept a connection
# This will wait until something connects to socket with port 4481
# Accept here is a blocking call; it blocks the current thread indefinitely
# until it receives a connection
connection, _ = server.accept
