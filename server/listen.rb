require "socket"

# Create a socket and bind it to port 4481.
socket = Socket.new(:INET, :STREAM)
addr = Socket.pack_sockaddr_in(4481, '0.0.0.0')
socket.bind(addr)

# Now we listen for incoming connections
# Set the listen queue to 5
socket.listen(5) 

# Still exits immediately.  Still one more
# step in the lifecycle
