require 'socket'

module CloudHash
  class Server
    def initialize(port)
      # Create the underlying server socket.
      @server = TCPServer.new(port)
      puts "Listening on port #{@server.local_address.ip_port}"
      @storage = {}
    end

    def start
      Socket.accept_loop(@server) do |connection|
        puts "Connection from #{connection.remote_address.ip_address}"
        handle(connection)
        connection.close
      end
    end

    def handle(connection)
      request = connection.read # Read until EOF
      connection.write process(request) # Write results back
    end

    # Supported commands: 
    # SET key value
    # GET key
    def process(request)
      command, key, value = request.split

      case command.upcase
      when 'GET'
        @storage[key]
      when 'SET'
        @storage[key] = value
        "SET #{key}"
      end
    end
  end
end

server = CloudHash::Server.new(4481)
server.start
