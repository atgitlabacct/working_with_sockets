require 'socket'

socket = TCPSocket.new('google.com', 80)
# Get an instance of Socket::Option representing the type of the socket.
opt = socket.getsockopt(Socket::SOL_SOCKET, Socket::SO_TYPE)
opt = socket.getsockopt(:SOCKET, :TYPE)

# Compare the integer representation of the option to the integer
# stored in Socket::SOCK_STREAM for comparison.
puts opt.inspect
puts "Socket::SOCK_STREAM: #{Socket::SOCK_STREAM}"
puts "Socket::SOCK_DGRAM: #{Socket::SOCK_DGRAM}"
opt.int == Socket::SOCK_STREAM
opt.int == Socket::SOCK_DGRAM
