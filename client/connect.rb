require 'socket'
require 'debugger'

#Initiate a connection to google.com on port 80
# socket = Socket.new(:INET, :STREAM)
# remote_addr = Socket.pack_sockaddr_in(80, 'google.com')
# socket.connect(remote_addr)
#
# Ruby Syntactic sugar
# socket = TCPSocket.new('google.com', 80)
#
# Can also take a block with Socket.tcp
Socket.tcp('google.com', 80) do |connection|
  connection.write "GET / HTTP/1.1\r\n"
  connection.close
end
