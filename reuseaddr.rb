require 'socket'

server = TCPServer.new('localhost', 4581)
server.setsockopt(:SOCKET, :REUSEADDR, true)

# Should be true
puts server.getsockopt(:SOCKET, :REUSEADDR)
